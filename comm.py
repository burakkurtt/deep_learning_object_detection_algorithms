# importing the requests library 
import requests 

# api-endpoint 
URL      = "https://37a2e518-2f10-426d-a92c-a18a5b5a1b46.mock.pstmn.io/"
USERNAME = 'ayasoft'
PASSWORD = '1234'
GIRIS    = "/api/giris"
CIKIS    = "/api/cikis"
FRAMELIST  = '/api/frame_listesi'
POSTRESULT  = '/api/cevap_gonder'

def login():
    PARAMS = {'kadi': USERNAME, 'sifre': PASSWORD}
    r = requests.post(url = URL+GIRIS, params = PARAMS)
    if r.status_code != 200:
        print("%s" %r)
        raise ValueError('Username or Password is not valid!')
    else:
        print("Login Success")


def logout():
    r = requests.get(url = URL+CIKIS)
    if r.status_code != 200:
        print("%s" %r)
        raise ValueError('An error occured!')
    else:
        print("Logout Success")

def getFrameList():
    r = requests.get(url = URL+FRAMELIST)
    if r.status_code != 200:
        print("%s" %r)
        raise ValueError('An error occured!')
    else:
        data = r.json()
        return data

def postResult(resultJson):
    r = requests.post(url = URL+POSTRESULT, params = resultJson)
    if r.status_code != 200:
        print("%s" %r)
        raise ValueError('An error occured!')
    else:
        print("Frame %s is sent" %resultJson['frame_id'])
