# Implementation of MobileNet using PyTorch
# Original code can be found;
# https://github.com/hoya012/pytorch-MobileNet
#
# Edited by
# Kurt | 2019
# hsynbrkkrt@gmail.com
#
# Trained with CIFAR10 Dataset 

import time
import torch
import torchvision
import torchvision.transforms as transforms
from torchvision.utils import save_image
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import os
import glob
import PIL
from PIL import Image
from torch.utils import data as D
from torch.utils.data.sampler import SubsetRandomSampler
import random

# Algorithm Parameters
batch_size = 16
validation_ratio = 0.1
epochs = 30

# IMAGE AUGMENTATIONTION
# Defining image transform for Train, Test and Validation dataset
transform_train = transforms.Compose([transforms.Resize(224),
                                        transforms.RandomCrop(224, padding=28),
                                        transforms.RandomHorizontalFlip(),
                                        transforms.ToTensor(),
                                        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2470, 0.2435, 0.2616))])

transform_validation = transforms.Compose([transforms.Resize(224),
                                            transforms.ToTensor(),
                                            transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2470, 0.2435, 0.2616))])

transform_test = transforms.Compose([transforms.Resize(224),
                                        transforms.ToTensor(),
                                        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2470, 0.2435, 0.2616))])

# Loading Dataset 
train_data = torchvision.datasets.CIFAR10(root='./CIFAR_dataset', 
                                          train=True,
                                          download=True, 
                                          transform=transform_train)

valid_data = torchvision.datasets.CIFAR10(root='./CIFAR_dataset',
                                          train=True,
                                          download=True,
                                          transform=transform_validation)

test_data = torchvision.datasets.CIFAR10(root='./CIFAR_dataset',
                                         train=False,
                                         download=True,
                                         transform=transform_test)

# Obtain training indices that will be used for validation
num_train = len(train_data)
indices = list(range(num_train))
np.random.shuffle(indices)

valid_split = int(np.floor(validation_ratio * num_train))
valid_idx, train_idx = indices[:valid_split], indices[valid_split:]

# Define samplers for obtaining trainig and validation batches
train_samplers = SubsetRandomSampler(train_idx)
valid_samplers = SubsetRandomSampler(valid_idx)

# Prepare data loader (Combine and sampler)
train_loader = torch.utils.data.DataLoader(train_data, 
                                            batch_size=batch_size, 
                                            sampler=train_samplers, 
                                            num_workers=0)
valid_loader = torch.utils.data.DataLoader(valid_data, 
                                            batch_size=batch_size, 
                                            sampler=valid_samplers, 
                                            num_workers=0)
test_loader = torch.utils.data.DataLoader(test_data, 
                                            batch_size=batch_size, 
                                            shuffle=False, 
                                            num_workers=0)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse',
           'ship', 'truck')

# CREATING MOBILE_NET LAYERS
# In order to create custom layer;
# https://pytorch.org/tutorials/beginner/pytorch_with_examples.html#pytorch-custom-nn-modules

class depthwise_conv(nn.Module):
    def __init__(self, n_in, kernel_size, padding, bias=False, stride=1):
        super(depthwise_conv, self).__init__()
        self.depthwise = nn.Conv2d( n_in,
                                    n_in,
                                    kernel_size=kernel_size, 
                                    stride=stride, 
                                    padding=padding, 
                                    groups=n_in, 
                                    bias=bias)
    def forward(self, x):
        out = self.depthwise(x)
        return(out)

class dw_block(nn.Module):
    def __init__(self, n_in, kernel_size, padding=1, bias=False, stride=1):
        super(dw_block, self).__init__()
        self.dw_block = nn.Sequential(  depthwise_conv(n_in, kernel_size, padding, bias, stride),
                                        nn.BatchNorm2d(n_in),
                                        nn.ReLU(True)) 
    def forward(self, x):
        out = self.dw_block(x)
        return out

class one_by_one_block(nn.Module):
    def __init__(self, n_in, n_out, padding=1, bias=False, stride=1):
        super(one_by_one_block, self).__init__()
        self.one_by_one_block = nn.Sequential(nn.Conv2d(n_in, n_out, kernel_size=1, stride=stride, padding=padding, bias=bias),
                                        nn.BatchNorm2d(n_out),
                                        nn.ReLU(True))
    def forward(self, x):
        out = self.one_by_one_block(x)
        return out

# MOBILE NET
class MobileNet(nn.Module):
    def __init__(self, input_channel, num_classes=10):
        super(MobileNet, self).__init__()
        self.MobileNet = nn.Sequential( nn.Conv2d(input_channel, 32, kernel_size=3, stride=2, padding=1, bias=False),
                                        nn.BatchNorm2d(32),
                                        nn.ReLU(True),
                                        
                                        dw_block(32, kernel_size=3),
                                        one_by_one_block(32, 64),
                                        
                                        dw_block(64, kernel_size=3, stride=2),
                                        one_by_one_block(64, 128),
                                        
                                        dw_block(128, kernel_size=3),
                                        one_by_one_block(128, 128),
                                        
                                        dw_block(128, kernel_size=3, stride=2),
                                        one_by_one_block(128, 256),
                                        
                                        dw_block(256, kernel_size=3),
                                        one_by_one_block(256, 256),
                                        
                                        dw_block(256, kernel_size=3, stride=2),
                                        one_by_one_block(256, 512),
                                        
                                        # 5 times 
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512),
                                        
                                        dw_block(512, kernel_size=3, stride=2),
                                        one_by_one_block(512, 1024),
                                        
                                        dw_block(1024, kernel_size=3, stride=2),
                                        one_by_one_block(1024, 1024),
                                        
                                        )
        
        self.linear = nn.Linear(1024, num_classes)

    def forward(self, x):
        out = self.MobileNet(x)
        avg_pool_out = F.adaptive_avg_pool2d(out, (1,1))
        avg_pool_flat = avg_pool_out.view(avg_pool_out.size(0), -1)
        output = self.linear(avg_pool_flat)
        return output

model = MobileNet(3, 10)


# TRAINING MODEL
# Settling device GPU or CPU
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model.to(device)
print('Used Device for training : ', device)

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.003)
valid_loss_min = np.Inf

for epochs in range(epochs):
    start = time.time()

    # Indicate to model, we train it
    model.train()

    train_loss = 0
    valid_loss = 0

    # Train dataset
    for inputs, labels in train_loader:
        # Move input and label tensors to default device 
        inputs, labels = inputs.to(device), labels.to(device)

        optimizer.zero_grad()

        prediction = model(inputs)
        loss = criterion(prediction, labels)
        loss.backward()
        optimizer.step()

        train_loss = train_loss + loss.item() 
    
    # Indicate to model, we evaluate it
    model.eval()

    with torch.no_grad():
        total = 0
        correct = 0
        # Validation dataset
        for inputs, labels in valid_loader: 
            inputs, labels = inputs.to(device), labels.to(device)
            prediction = model.forward(inputs)
            batch_loss = criterion(prediction, labels)

            valid_loss = valid_loss + batch_loss.item()

            # Calculate accuracy
            _, predicted = torch.max(prediction.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

        valid_accuracy = 100 * correct / total    

    # Calculate average loss
    train_loss = train_loss / len(train_loader)
    valid_loss = valid_loss / len(valid_loader)

    print('Epoch:{} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f} \t Validation Accuracy: {:.6f}'.format(epochs+1,
                                                                                                             train_loss,
                                                                                                             valid_loss,
                                                                                                             valid_accuracy))
    
    # Save model
    if valid_loss <= valid_loss_min:
        print('Validation Loss decreased ({:.6f} --> {:.6f}). Saving model ...' .format(valid_loss_min,
                                                                                         valid_loss))
        path = "./cifar10.pt"
        torch.save(model.state_dict(), path)
        valid_loss_min = valid_loss
    
    print(f"Time per epoch: {(time.time() - start):.3f} seconds")



def test_model(model, criterion):
    # Monitor the test loss and accuracy
    correct = 0.0
    total = 0.0

    with torch.no_grad():
        for data in test_loader:
            images, labels = data
            images, labels = images.to(device), labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    print('\nTest Accuracy: %2d%% (%2d/%2d)' % (100. * correct / total, correct, total))

# Testting Trained Model
test_model(model, criterion)