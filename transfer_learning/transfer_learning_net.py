# This repo uses pretrained model to train new 
# dataset.
# Original Code is taken from https://heartbeat.fritz.ai/transfer-learning-with-pytorch-cfcb69016c72
# 
# Edited by
# Burak KURT | 2019
# e-mail : hsynbrkkrt@gmail.com

# Image Dataset should be below format and modify the dataset path in code
# Dataset_name
# |____Class_name1
# |    |__image1
# |    |__image2
# |____Class_name2
# |    |__image1
# |    |__image2

import time
import torch
from torch import nn
import torch.nn.functional as F
from torch import optim
from torch.autograd import Variable
from matplotlib import pyplot as plt
from torchvision import datasets, transforms, models
from torch.utils.data.sampler import SubsetRandomSampler
from PIL import Image
import numpy as np 
import os

img_dir='./cell_images'

def img_show(image):
    """Display image"""
    plt.figure(figsize=(6, 6))
    plt.imshow(image)
    plt.axis('off')
    plt.show()

# IMAGE AUGMATION
# Deffining image transform for Train, Test and Validation dataset
train_transform = transforms.Compose([transforms.RandomResizedCrop(size=256, scale=(0.8, 1.0)),
                                        transforms.RandomRotation(degrees=15),
                                        transforms.ColorJitter(),
                                        transforms.CenterCrop(size=224),  # ImageaNet standart
                                        transforms.ToTensor(),
                                        transforms.Normalize([0.485, 0.456, 0.406],
                                                             [0.229, 0.224, 0.225])
                                        ])

test_transform = transforms.Compose([transforms.Resize(size=256),
                                        transforms.CenterCrop(size=224),
                                        transforms.ToTensor(),
                                        transforms.Normalize([0.485, 0.456, 0.406],
                                                             [0.229, 0.224, 0.225])
                                        ])


validation_transform = transforms.Compose([transforms.Resize(size=256),
                                            transforms.CenterCrop(size=224),
                                            transforms.ToTensor(),
                                            transforms.Normalize([0.485, 0.456, 0.406],
                                                                 [0.229, 0.224, 0.225])
                                            ])

# Loading Dataset 
train_data = datasets.ImageFolder(img_dir, transform=train_transform)
# Number of subprocesses to use for data loading
num_workers = 0
# percentage of training set to use as validation
valid_size = 0.2
test_size = 0.1

# Obtain training indices that will be used for validation
num_train = len(train_data)
indices = list(range(num_train))
np.random.shuffle(indices)

valid_split = int(np.floor((valid_size) * num_train))
test_split = int(np.floor((valid_size + test_size) * num_train))

valid_idx, test_idx, train_idx = indices[:valid_split], indices[valid_split:test_split], indices[test_split:]

# Define samplers for obtaining trainig and validation batches
train_samplers = SubsetRandomSampler(train_idx)
test_samplers = SubsetRandomSampler(test_idx)
valid_samplers = SubsetRandomSampler(valid_idx)

# Prepare data loader (Combine and sampler)
train_loader = torch.utils.data.DataLoader(train_data, batch_size=32, sampler=train_samplers, num_workers=num_workers)
valid_loader = torch.utils.data.DataLoader(train_data, batch_size=32, sampler=valid_samplers, num_workers=num_workers)
test_loader = torch.utils.data.DataLoader(train_data, batch_size=32, sampler=test_samplers, num_workers=num_workers)

# MODEL
# Loading pretrained model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = models.densenet121(pretrained=True)
# print(model)

# Freezing conv layers and replacing the fully connected layers with custom classifier
for param in model.parameters():
    param.require_grad = False

fully_connected = nn.Sequential(
    nn.Linear(1024, 460),
    nn.ReLU(),
    nn.Dropout(0.4),

    nn.Linear(460, 2),
    nn.LogSoftmax(dim=1)
)

model.classifier = fully_connected
criterion = nn.NLLLoss() 
optimizer = torch.optim.Adam(model.classifier.parameters(), lr=0.003)
model.to(device)

# TRAINING MODEL
epochs = 5
valid_loss_min = np.Inf

for epochs in range(epochs):
    start = time.time()

    # Indicate to model, we train it
    model.train()

    train_loss = 0
    valid_loss = 0

    # Train dataset
    for inputs, labels in train_loader:
        # Move input and label tensors to default device 
        inputs, labels = inputs.to(device), labels.to(device)

        optimizer.zero_grad()

        prediction = model(inputs)
        loss = criterion(prediction, labels)
        loss.backward()
        optimizer.step()

        train_loss = train_loss + loss.item() 
    
    # Indicate to model, we evaluate it
    model.eval()

    with torch.no_grad():
        accuracy = 0
        # Validation dataset
        for inputs, labels in valid_loader: 
            inputs, labels = inputs.to(device), labels.to(device)
            prediction = model.forward(inputs)
            batch_loss = criterion(prediction, labels)

            valid_loss = valid_loss + batch_loss.item()

            # Calculate accuracy
            ps = torch.exp(prediction)
            top_p, top_class = ps.topk(1, dim=1)
            equals = top_class == labels.view(*top_class.shape)
            accuracy += torch.mean(equals.type(torch.FloatTensor)).item()

    # Calculate average loss
    train_loss = train_loss / len(train_loader)
    valid_loss = valid_loss / len(valid_loader)
    valid_accuracy = accuracy / len(valid_loader)

    print('Epoch:{} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f} \t Validation Accuracy: {:.6f}'.format(epochs+1,
                                                                                                             train_loss,
                                                                                                             valid_loss,
                                                                                                             valid_accuracy))
    
    # Save model
    if valid_loss <= valid_loss_min:
        print('Validation Loss decreased ({:.6f} --> {:.6f}). Saving model ...' .format(valid_loss_min,
                                                                                         valid_loss))
        path = "./malaria.pt"
        torch.save(model.state_dict(), path)
        valid_loss_min = valid_loss
    
    print(f"Time per epoch: {(time.time() - start):.3f} seconds")

def test(model, criterion):
    # Monitor the test loss and accuracy
    test_loss = 0.0
    correct = 0.0
    total = 0.0

    for batch_idx, (data, target) in enumerate(test_loader):
        # move to GPU
        if torch.cuda.is_available():
            data, target = data.cuda(), target.cuda()
        
        prediction = model(data)
        loss = criterion(prediction, target)
        # Update average test loss
        test_loss = test_loss + ((1 / (batch_idx + 1)) * (loss.data - test_loss))
        # Convert output probabilites to predicted clas
        pred = prediction.data.max(1, keepdim=True)[1]
        correct += np.sum(np.squeeze(pred.eq(target.data.view_as(pred))).cpu().numpy())
        total += data.size(0)

        print('Test Loss: {:.6f}\n'.format(test_loss))
        print('\nTest Accuracy: %2d%% (%2d/%2d)' % (100. * correct / total, correct, total))

# Testting Trained Model
test(model, criterion)



        

