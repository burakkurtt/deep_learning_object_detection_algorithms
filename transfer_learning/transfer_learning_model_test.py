from glob import glob
from PIL import Image
from termcolor import colored
import numpy as np
import torch
import torch.nn as nn
from torchvision import transforms, models
from matplotlib import pyplot as plt

img_dir = './cell_images/'

# Import used model and update its parameter from saved state_dict
model = models.densenet121(pretrained=True)
fully_connected = nn.Sequential(
    nn.Linear(1024, 460),
    nn.ReLU(),
    nn.Dropout(0.4),

    nn.Linear(460, 2),
    nn.LogSoftmax(dim=1)
)
model.classifier = fully_connected

state_dict = torch.load('malaria.pt')
model.load_state_dict(state_dict)

class_names = ['parasitized', 'uninfected']
inf = np.array(glob(img_dir + "parasitized/*"))
uinf = np.array(glob(img_dir + "uninfected/*"))


def predict_malaria(model, class_names, img_path):
    img = load_input_image(img_path)
    model = model.cpu()
    model.eval()
    idx = torch.argmax(model(img))
    return class_names[idx]

def load_input_image(img_path):
    image = Image.open(img_path)
    prediction_transform = transforms.Compose([transforms.Resize(size=(224,224)),
                                               transforms.ToTensor(),
                                               transforms.Normalize([0.485, 0.456, 0.406], 
                                                                  [0.229, 0.224, 0.225])])
    # Discard the transparent, alpha channel (that's the :3) and add the batch dimension
    image = prediction_transform(image)[:3,:,:].unsqueeze(0)
    return image

for i in range(3):
    img_path = inf[i]
    img = Image.open(img_path)
    if predict_malaria(model, class_names, img_path) == 'parasitized':
        print(colored('parasitized', 'green'))
    else:
        print(colored('uninfected', 'red'))    
    plt.imshow(img)
    plt.show()

for i in range(3):
    img_path = uinf[i]
    img = Image.open(img_path)
    if predict_malaria(model, class_names, img_path) == 'uninfected':
        print(colored('uninfected', 'green'))
    else:
        print(colored('parasitized', 'red'))    
    plt.imshow(img)
    plt.show()
