import json
import simplejson



class JsonOutput:
    def __init__(self, filepath = 'output.json'):
        super(JsonOutput, self).__init__()
        self.data = {}
        self.data['nesne'] = []
        self.path = filepath


    def append(self, frameID, frameZaman, x1, y1, x2, y2, tur):
        self.data['nesne'].append({
            'frameid': frameID,
            'framezaman': frameZaman,
            'x1': x1,
            'y1': y1,
            'x2': x2,
            'y2': y2,
            'tur': tur
        })
        with open(self.path, 'w') as json_file:  
            json.dump(self.data, json_file, indent=4)

    def save(path = self.path):
        with open(path, 'w') as json_file:  
            json.dump(self.data, json_file, indent=4)
