import math
import itertools

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init

from torch.autograd import Variable
from mobile_net import MobileNet
from multibox_layer import MultiBoxLayer


class L2Norm2d(nn.Module):
    '''L2Norm layer across all channels.'''
    def __init__(self, scale):
        super(L2Norm2d, self).__init__()
        self.scale = scale

    def forward(self, x, dim=0):
        '''out = scale * x / sqrt(sum x_i^2)'''
        return self.scale * x * x.pow(2).sum(dim).clamp(min=1e-12).rsqrt().expand_as(x)


class SSD300(nn.Module):
    input_size = 300
    
    def __init__(self):
        super(SSD300, self).__init__()

        self.base = MobileNet()
        self.norm4 = L2Norm2d(20)

        self.conv5_1 = nn.Conv2d(512, 512, kernel_size=3, padding=1)
        self.conv5_2 = nn.Conv2d(512, 512, kernel_size=3, padding=1)
        self.conv5_3 = nn.Conv2d(512, 512, kernel_size=3, padding=1)

        self.conv6 = nn.Conv2d(512, 1024, kernel_size=3, padding=1, stride=2)

        self.conv7 = nn.Conv2d(1024, 1024, kernel_size=1, padding=0)

        self.conv8_1 = nn.Conv2d(1024, 256, kernel_size=1, padding=0, stride=1)
        self.conv8_2 = nn.Conv2d(256, 512, kernel_size=3, padding=1, stride=2)

        self.conv9_1 = nn.Conv2d(512, 128, kernel_size=1, padding=0, stride=1)
        self.conv9_2 = nn.Conv2d(128, 256, kernel_size=3, padding=1, stride=2)

        self.conv10_1 = nn.Conv2d(256, 128, kernel_size=1, padding=0, stride=1)
        self.conv10_2 = nn.Conv2d(128, 256, kernel_size=3, padding=0, stride=1)

        self.conv11_1 = nn.Conv2d(256, 128, kernel_size=1, padding=0, stride=1)
        self.conv11_2 = nn.Conv2d(128, 256, kernel_size=3, padding=0, stride=1)

        # multibox layer
        self.multibox = MultiBoxLayer()

    def forward(self, x):
        hs = []
        h = self.base(x)
        hs.append(self.norm4(h))        # conv4_3

        h = F.relu(self.conv5_1(h))
        h = F.relu(self.conv5_2(h))
        h = F.relu(self.conv5_3(h))

        h = F.relu(self.conv6(h))       # conv6
        h = F.relu(self.conv7(h))       # conv7
        hs.append(h)                    

        h = F.relu(self.conv8_1(h))
        h = F.relu(self.conv8_2(h))
        hs.append(h)                    # conv8_2

        h = F.relu(self.conv9_1(h))
        h = F.relu(self.conv9_2(h))
        hs.append(h)                    # conv9_2


        h = F.relu(self.conv10_1(h))
        h = F.relu(self.conv10_2(h))
        hs.append(h)                    # conv10_2

        h = F.relu(self.conv11_1(h))
        h = F.relu(self.conv11_2(h))
        hs.append(h)                    # conv11_2

        loc_preds, conf_preds = self.multibox(hs)
        return loc_preds, conf_preds



