# SSD300 Multibox object detection implementation using mobile network as base network.
# SSD needs to dataset with ground truth boxes position and class name.
#
#  Burak KURT | 2019
# e-mail : hsynbrkkrt@gmail.com

import time
import torch
from torch import nn
import torch.nn.functional as F
from torch import optim
from torch.autograd import Variable
from matplotlib import pyplot as plt
from torchvision import datasets, transforms, models
from torch.utils.data.sampler import SubsetRandomSampler
from PIL import Image
import numpy as np 
import os
from ssd_models import SSD300
from datagen import ListDataset
from multibox_loss import MultiBoxLoss

# DATASET PREPARETION
print('==> Preparing data..')
transform = transforms.Compose([transforms.ToTensor(),
                                transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))])

# percentage of training set to use as validation
valid_size = 0.2
train_size = 0.8

trainset = ListDataset(root='./VOC2012/train/JPEGImages', list_file='./voc_data/voc12_train_light.txt', train=True, transform=transform)
testset = ListDataset(root='./VOC2012/test/JPEGImages', list_file='./voc_data/voc12_test_light.txt', train=False, transform=transform)

# Obtain training indices that will be used for validation
num_train = len(trainset)
indices = list(range(num_train))
np.random.shuffle(indices)

# Splitting train dataset
valid_split = int(np.floor((valid_size) * num_train))
valid_idx, train_idx = indices[:valid_split], indices[valid_split:]

# Define samplers for obtaining trainig and validation batches
train_samplers = SubsetRandomSampler(train_idx)
valid_samplers = SubsetRandomSampler(valid_idx)

train_loader = torch.utils.data.DataLoader(trainset, batch_size=5, shuffle=False, sampler=train_samplers, num_workers=4)
valid_loader = torch.utils.data.DataLoader(trainset, batch_size=5, shuffle=False, sampler=valid_samplers, num_workers=4)
test_loader = torch.utils.data.DataLoader(testset, batch_size=5, shuffle=False, num_workers=4)

# MODEL
print('==> Creating data..')
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = SSD300()
# Model criterions
criterion = MultiBoxLoss()
optimizer = optim.SGD(model.parameters(), lr=0.0001, momentum=0.9, weight_decay=1e-4)
model.to(device)

# TRAINING MODEL
epochs = 1
valid_loss_min = np.Inf

for epochs in range(epochs):
    start = time.time()

    # Indicate to model, we train it
    model.train()

    train_loss = 0
    valid_loss = 0

    # Train dataset
    for batch_idx, (images, loc_targets, conf_targets) in enumerate(train_loader):
        if torch.cuda.is_available():
            # Move input and label tensors to default device 
            images, loc_targets, conf_targets = images.cuda(), loc_targets.cuda(), conf_targets.cuda()

        images = Variable(images)
        loc_targets = Variable(loc_targets)
        conf_targets = Variable(conf_targets)

        optimizer.zero_grad()
        loc_preds, conf_preds = model(images) 
        loss_tr = criterion(loc_preds, loc_targets, conf_preds, conf_targets)
        loss_tr.backward()
        optimizer.step()

        train_loss = train_loss + loss_tr.item() 

    # Indicate to model, we evaluate it
    model.eval()

    with torch.no_grad():
        accuracy = 0
        # Validation dataset
        for batch_idx, (images, loc_targets, conf_targets) in enumerate(valid_loader):
            if torch.cuda.is_available():
                # Move input and label tensors to default device 
                images, loc_targets, conf_targets = images.cuda(), loc_targets.cuda(), conf_targets.cuda()

            images = Variable(images)
            loc_targets = Variable(loc_targets)
            conf_targets =Variable(conf_targets)

            loc_preds, conf_preds = model(images)  

            loss_v = criterion(loc_preds, loc_targets, conf_preds, conf_targets)  

            valid_loss = valid_loss + loss_v.item()

    # Calculate average loss
    train_loss = train_loss / len(train_loader)
    valid_loss = valid_loss / len(valid_loader)

    print('Epoch:{} \tTraining Loss: {:.6f} \tValidation Loss: {:.6f} \n'.format(epochs+1,
                                                                                 train_loss,
                                                                                 valid_loss))
                                                                                                            
    print('Time per epoch: {:.3f} seconds'.format(time.time() - start))

path = "./ssd_mobileNet_voc2012.pt"
torch.save(model.state_dict(), path)

def test(model, criterion):
    # Monitor the test loss and accuracy
    test_loss = 0

    for batch_idx, (images, loc_targets, conf_targets) in enumerate(test_loader):
        # move to GPU
        if torch.cuda.is_available():
                # Move input and label tensors to default device 
                images, loc_targets, conf_targets = images.cuda(), loc_targets.cuda(), conf_targets.cuda()
        
        images = Variable(images)
        loc_targets = Variable(loc_targets)
        conf_targets = Variable(conf_targets)

        loc_preds, conf_preds = model(images)
        loss_t = criterion(loc_preds, loc_targets, conf_preds, conf_targets)
        # Update average test loss
        test_loss = test_loss + loss_t.item()

    test_loss = test_loss / len(test_loader)

    print('Test Loss: {:.6f}\n'.format(test_loss))

# Testting Trained Model
test(model, criterion)
