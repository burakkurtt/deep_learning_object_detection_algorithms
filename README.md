# Deep_learning_object_detection_algorithms

Object detection libraries that uses Deep learning techniques for Teknofest competition 2019

1 - data_prepration;

	This library is used for preprocessing of images / dataset
    This class takes source and destination directions of images, images pre-processing 
    methods have beed applied to them. 
    *** Each class of images needs to be grouped inside folder named with belonging class. 
    Dataset
     - Drone
        - drone1.png
        - drone2.png
     - FixedWing
        - fixedWing1.png
        - fixedWing2.png

2 - transfer_learning;

	This script is example of transfer learning method of Deep learning.

3 - mobile_net

	This script is example of SSD net, Cifar10 dataset is used for training and Google Colabs is used for training this dataset.
	run.ipynb file used for google Colabs trainig procedure.

4 - ssd_mobileNet

	This script is used SSD model training and Mobile net structure is used as base network of SSD structure. 

	Used Dataset is VOC_2012 dataset;
	Download link;
	train data : http://host.robots.ox.ac.uk/pascal/VOC/voc2012/#data
	test data  : http://host.robots.ox.ac.uk:8080/eval/challenges/voc2012/

	Download dataset and change the direction of them in "ssd_mobileNet_train.py" file and train it, it saved trained model 
	to current directory.

5 - main_pretrainedSSD
