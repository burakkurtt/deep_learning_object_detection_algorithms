""" 
    Image Pre-processing class;
    This class takes source and destination directions of images, images pre-processing 
    methods have beed applied to them. 
    *** Each class of images needs to be grouped inside folder named with belonging class. 
    Dataset
     - Drone
        - drone1.png
        - drone2.png
     - FixedWing
        - fixedWing1.png
        - fixedWing2.png

    --------------------
    Burak KURT
    e-mail   :  hsynbrkkrt@gmail.com.com  
"""

import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from shutil import copyfile

class ImgPreProcessor:
    def __init__(self, source_directory, destination_directory):
        self.source_dir = source_directory
        self.destination_dir = destination_directory

    def check_file_exist(self, file_dest_dir):
        if os.path.isdir(file_dest_dir):
            pass
        else:
            os.makedirs(file_dest_dir)

    def img_labeler(self):
        images = []
        labels = []
        for files in os.listdir(self.source_dir):
            file_dir = os.path.join(self.source_dir, files)
            for file_name in os.listdir(file_dir):
                img_dir = os.path.join(file_dir, file_name) 
                images.append(img_dir)
                labels.append(files)
        return images, labels               
        
    def img_renamer(self):
        for files in os.listdir(self.source_dir):
            file_dir = os.path.join(self.source_dir, files)
            dest_dir = os.path.join(self.destination_dir, files)
            self.check_file_exist(dest_dir)
            for i, file_name in enumerate(os.listdir(file_dir)):
                file_type = file_name.split('.')
                old_img_dir = os.path.join(file_dir, file_name)
                new_img_dir = os.path.join(dest_dir, files+'_'+str(i+1)+'.'+file_type[-1])
                copyfile(old_img_dir, new_img_dir)
                
    def img_resizer(self, image_size=(50, 50)):
        for files in os.listdir(self.source_dir):
            file_dir = os.path.join(self.source_dir, files)
            dest_dir = os.path.join(self.destination_dir, files)
            self.check_file_exist(dest_dir)
            for file_name in os.listdir(file_dir):
                img_directory = os.path.join(file_dir, file_name) 
                original_img = cv2.imread(img_directory)

                h, w = original_img.shape[:2]
                sh, sw = image_size
                # interpolation method
                if h > sh or w > sw:  # shrinking image
                    interp = cv2.INTER_AREA
                else: # stretching image
                    interp = cv2.INTER_CUBIC

                # aspect ratio of image
                aspect = w/h

                # padding
                if aspect > 1: # horizontal image
                    new_shape = list(original_img.shape)
                    new_shape[0] = w
                    new_shape[1] = w
                    new_shape = tuple(new_shape)
                    new_img=np.zeros(new_shape, dtype=np.uint8)
                    h_offset=int((w-h)/2)
                    new_img[h_offset:h_offset+h, :, :] = original_img.copy()

                elif aspect < 1: # vertical image
                    new_shape = list(original_img.shape)
                    new_shape[0] = h
                    new_shape[1] = h
                    new_shape = tuple(new_shape)
                    new_img = np.zeros(new_shape,dtype=np.uint8)
                    w_offset = int((h-w) / 2)
                    new_img[:, w_offset:w_offset + w, :] = original_img.copy()
                else:
                    new_img = original_img.copy()

                # scale and pad
                scaled_img = cv2.resize(new_img, image_size, interpolation=interp)

                #saving images
                saving_directory = os.path.join(dest_dir, file_name) 
                cv2.imwrite(saving_directory, scaled_img)

    def img_grayscaler(self):
        for files in os.listdir(self.source_dir):
            file_dir = os.path.join(self.source_dir, files)
            dest_dir = os.path.join(self.destination_dir, files)
            self.check_file_exist(dest_dir)
            for file_name in os.listdir(file_dir):
                img_directory = os.path.join(file_dir, file_name)
                original_img = cv2.imread(img_directory)
                img_grayscale = cv2.cvtColor(original_img, cv2.COLOR_BGR2GRAY) 
                
                #saving images
                saving_directory = os.path.join(dest_dir, file_name) 
                cv2.imwrite(saving_directory, img_grayscale)
                
        
    def img_normalizer(self, norm_type='std'):
        # 'minmax'  - Normalize image to [0 1]
        # 'std'     - Centered image subsract the standart deviation
        for files in os.listdir(self.source_dir):
            file_dir = os.path.join(self.source_dir, files)
            dest_dir = os.path.join(self.destination_dir, files)
            self.check_file_exist(dest_dir)
            for file_name in os.listdir(file_dir):
                img_directory = os.path.join(file_dir, file_name)
                img_org = cv2.imread(img_directory)
                if norm_type == 'minmax':
                    img_norm = img_org * 255.0/np.max(img_org)
                elif norm_type == 'std':
                    img_norm = img_org / np.std(img_org, axis=0)
                 
                #saving images
                saving_directory = os.path.join(dest_dir, file_name) 
                cv2.imwrite(saving_directory, img_norm)

    def img_mean_subtracter(self):
        for files in os.listdir(self.source_dir):
            file_dir = os.path.join(self.source_dir, files)
            dest_dir = os.path.join(self.destination_dir, files)
            self.check_file_exist(dest_dir)
            for file_name in os.listdir(file_dir):
                img_directory = os.path.join(file_dir, file_name)
                original_img = cv2.imread(img_directory)
                mean_subtract_img = original_img - np.mean(original_img)   
            
                #saving images
                saving_directory = os.path.join(dest_dir, file_name) 
                cv2.imwrite(saving_directory, mean_subtract_img)

           

if __name__ == '__main__':

    # In order to label images
    source_dir = './dataset/'
    destination_dir = './labeled_img/'
    img_preprocessor_label = ImgPreProcessor(source_dir, destination_dir)
    img, label = img_preprocessor_label.img_labeler()
    
    # In order to rename images
    source_dir = './dataset/'
    destination_dir = './labeled_img/'
    img_preprocessor = ImgPreProcessor(source_dir, destination_dir)
    img_preprocessor.img_renamer()

    # In order to resize images
    source_dir = './dataset/'
    destination_dir = './resized_img/'
    img_preprocessor = ImgPreProcessor(source_dir, destination_dir)
    img_preprocessor.img_resizer((32,32))

    # In order to grayscale images
    source_dir = './dataset/'
    destination_dir = './grayscale_img/'
    img_preprocessor = ImgPreProcessor(source_dir, destination_dir)
    img_preprocessor.img_grayscaler()

    # In order to normalize images
    source_dir = './dataset/'
    destination_dir = './normalized_img/'
    img_preprocessor = ImgPreProcessor(source_dir, destination_dir)
    img_preprocessor.img_normalizer()

    # In order to mean subtracted images
    source_dir = './dataset/'
    destination_dir = './mean_subtracted_img/'
    img_preprocessor = ImgPreProcessor(source_dir, destination_dir)
    img_preprocessor.img_mean_subtracter()
    

    
    
    

