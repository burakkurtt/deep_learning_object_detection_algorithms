import sys
sys.path.append('./ssd_model')

import cv2
import sys
import argparse
from PIL import Image, ImageDraw, ImageFont
from ssd_models import SSD300
from encoder import DataEncoder

import torch
import torchvision
import torch.nn.functional as F
import torchvision.transforms as transforms
import numpy as np
from torch.autograd import Variable

if __name__ == '__main__' :
    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--video", type=str,
        help="path to input video file")
    ap.add_argument("-c", "--cam", type=int,
        help="device number for camera eg. '0' for '/dev/video0'")
    args = vars(ap.parse_args())
    vid = 0
    if args.get("video", False):
        vid = args["video"]
    # otherwise, grab a reference to the video file
    elif args.get("cam", False):
        vid = args["cam"]

    # Load Model
    model = SSD300()
    model.load_state_dict(torch.load('ssd_model/ssd_mobileNet_voc2012.pt'))
    model.eval()

    # Read video
    video = cv2.VideoCapture(vid)
 
    # Exit if video not opened.
    if not video.isOpened():
        print ("Could not open video")
        sys.exit()
 
    # Read first frame.
    ok, frame_cv = video.read()
    if not ok:
        print ('Cannot read video file')
        sys.exit()

    while True:
        # Read a new frame
        ok, frame_cv = video.read()
        if not ok:
            break

        # Start timer
        timer = cv2.getTickCount()

        # You may need to convert the color.
        # opencv uses color_BGR
        # PIL uses RGB
        frame_cv = cv2.cvtColor(frame_cv, cv2.COLOR_BGR2RGB)
        frame_pil = Image.fromarray(frame_cv)

        frame_pil_s = frame_pil.resize((300, 300))
        transform = transforms.Compose([transforms.ToTensor(),
                                transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))])
        frame_pil_s = transform(frame_pil_s)

        # Forward Model
        with torch.no_grad():
            if torch.cuda.is_available():
                frame_pil_s = frame_pil_s.cuda()
            else:
                print("there is no cuda, so cpu is used")
            loc, conf = model(Variable(frame_pil_s[None,:,:,:]))
    
        # Decode
        data_encoder = DataEncoder()
        boxes, labels, scores = data_encoder.decode(loc.data.squeeze(0), F.softmax(conf.squeeze(0), dim=1).data)

        draw = ImageDraw.Draw(frame_pil)
        for i, box in enumerate(boxes):
            box[::2] *= frame_pil.width
            box[1::2] *= frame_pil.height
            draw.rectangle(list(box), outline='red')

        frame_cv = cv2.cvtColor(np.array(frame_pil), cv2.COLOR_RGB2BGR)
        
        # Calculate Frames per second (FPS)
        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer)
        # Display FPS on frame
        cv2.putText(frame_cv, "FPS : " + str(fps), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2);
 
        # Display result
        cv2.imshow("Video", frame_cv)
 
        # Exit if ESC pressed
        k = cv2.waitKey(1) & 0xff
        if k == 27 : break
