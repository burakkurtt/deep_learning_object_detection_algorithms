import torch
import torch.nn as nn
import torch.nn.functional as F

class depthwise_conv(nn.Module):
    def __init__(self, n_in, kernel_size, padding, bias=False, stride=1):
        super(depthwise_conv, self).__init__()
        self.depthwise = nn.Conv2d( n_in,
                                    n_in,
                                    kernel_size=kernel_size, 
                                    stride=stride, 
                                    padding=padding, 
                                    groups=n_in, 
                                    bias=bias)
    def forward(self, x):
        out = self.depthwise(x)
        return(out)

class dw_block(nn.Module):
    def __init__(self, n_in, kernel_size, padding=1, bias=False, stride=1):
        super(dw_block, self).__init__()
        self.dw_block = nn.Sequential(  depthwise_conv(n_in, kernel_size, padding, bias, stride),
                                        nn.BatchNorm2d(n_in),
                                        nn.ReLU(True)) 
    def forward(self, x):
        out = self.dw_block(x)
        return out

class one_by_one_block(nn.Module):
    def __init__(self, n_in, n_out, padding=1, bias=False, stride=1):
        super(one_by_one_block, self).__init__()
        self.one_by_one_block = nn.Sequential(nn.Conv2d(n_in, n_out, kernel_size=1, stride=stride, padding=padding, bias=bias),
                                        nn.BatchNorm2d(n_out),
                                        nn.ReLU(True))
    def forward(self, x):
        out = self.one_by_one_block(x)
        return out

# MOBILE NET
class MobileNet(nn.Module):
    
    def __init__(self):

        super(MobileNet, self).__init__()
        self.MobileNet = nn.Sequential( nn.Conv2d(3, 32, kernel_size=3, stride=2, padding=1, bias=False),
                                        nn.BatchNorm2d(32),
                                        nn.ReLU(True),
                                        
                                        dw_block(32, kernel_size=3),                            # 150x150x32
                                        one_by_one_block(32, 64, stride=1, padding=0),          # 150x150x64
                                        
                                        dw_block(64, kernel_size=3, stride=2),                  # 75x75x64
                                        one_by_one_block(64, 128, stride=1, padding=0),         # 75x75x128
                                        
                                        dw_block(128, kernel_size=3),                           # 75x75x128
                                        one_by_one_block(128, 128, stride=1, padding=0),        # 75x75x128    
                                        
                                        dw_block(128, kernel_size=3, stride=2),                 # 38x38x128 
                                        one_by_one_block(128, 256, stride=1, padding=0),        # 38x38x256
                                        
                                        dw_block(256, kernel_size=3),
                                        one_by_one_block(256, 256, stride=1, padding=0),
                                        
                                        dw_block(256, kernel_size=3),
                                        one_by_one_block(256, 512, stride=1, padding=0),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512, stride=1, padding=0),

                                        # 5 times 
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512, stride=1, padding=0),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512, stride=1, padding=0),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512, stride=1, padding=0),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512, stride=1, padding=0),
                                        dw_block(512, kernel_size=3),
                                        one_by_one_block(512, 512, stride=1, padding=0),
                                        
                                        )

    def forward(self, x):
        out = self.MobileNet(x)
        return out